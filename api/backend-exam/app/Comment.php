<?php

namespace App;

use App\Post;
use Illuminate\Database\Eloquent\Model;
use App\Transformers\CommentTransformer;

class Comment extends Model
{
    
    public $transformer = CommentTransformer::class;

	protected $fillable = [
        'body',
        'creator_id',
        'commentable_id', 
        'parent_id',
    ];

    //relatioship to user
    public function user()
    {
        return $this->belongsTo(User::class , 'creator_id');
    }

    //relatioship to post
    public function post()
    {
        return $this->belongsTo(Post::class, 'commentable_id');
    }
}
