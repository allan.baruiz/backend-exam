<?php

namespace App;

use App\User;
use App\Comment;
use App\Transformers\PostTransformer;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    public $transformer = PostTransformer::class;

    protected $fillable = [
        'title',
        'content',
        'slug',
        'user_id', 
    ];

    //relatioship to user
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    //relatioship to user
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
