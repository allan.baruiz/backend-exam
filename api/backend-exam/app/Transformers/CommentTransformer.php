<?php

namespace App\Transformers;

use App\Comment;
use League\Fractal\TransformerAbstract;

class CommentTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Comment $comment)
    {
        return [
               
            'body' => (string)$comment->body,
            'creator_id' => (int)$comment->creator_id,
            'creator_type' => "App\User",
            'commentable_id' => (int)$comment->commentable_id,
            'commentable_type' => "App\Post",
            '_lft' => 1,
            '_rgt' => 2,
            'updated_at' => (string)$comment->updated_at,
            'created_at' => (string)$comment->created_at,
            'id' => (int)$comment->id,
            
        ];
    }
}
