<?php

namespace App\Transformers;

use App\Post;
use League\Fractal\TransformerAbstract;

class PostTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Post $post)
    {
        return [
             
            'title' => (string)$post->title,
            'content' => (string)$post->content,
            'slug' => (string)$post->slug,
            'updated_at' => (string)$post->updated_at,
            'created_at' => (string)$post->created_at,
            'id' => (int)$post->id,
            'user_id' => (int)$post->user_id,
            
            'links' => [
                [
                    'comment' => url('posts/'. $post->id .'/comments'),
                ],
            ]
        ];
    }
}

