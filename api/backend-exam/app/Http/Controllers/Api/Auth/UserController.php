<?php

namespace App\Http\Controllers\Api\Auth;

use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use JWTAuthException;

class UserController extends Controller
{
    private $user;

    public function __construct(User $user){
        $this->user = $user;
    }

    // Register User
    public function register(Request $request){

        $user = $this->user->create([
          'name' => $request->get('name'),
          'email' => $request->get('email'),
          'password' => bcrypt($request->get('password'))
        ]);

        return response()->json(['status'=>true,'message'=>'User created successfully','data'=>$user]);
    }
    
    // Login User
    public function login(Request $request){

        $credentials = $request->only('email', 'password');

        $token = null;

        try {
           if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['invalid_email_or_password'], 422);
           }
        } catch (JWTAuthException $e) {
            return response()->json(['failed_to_create_token'], 500);
        }

        return response()->json(compact('token'));
    }

    //Authenticate User
    public function getAuthUser(Request $request){
        
    	 try{
            $user = JWTAuth::parseToken()->toUser();
            if(! $user){
                return response()->json(['error' => 'User not found'], 401);
                //return $this->response->errorNotFound("User not found");
            }
        } catch(\Tymon\JWTAutH\Exceptions\TokenInvalidException $ex){
            return response()->json(['error' => 'Token is invalid'], 401);
        } catch(\Tymon\JWTAutH\Exceptions\TokenExpiredException $ex){
            return response()->json(['error' => 'Token has expired'], 401);
        } catch(\Tymon\JWTAutH\Exceptions\TokenBlacklistedException $ex){
            return response()->json(['error' => 'Token is blacklisted'], 401);
        }
        
        
        return response()->json(['result' => $user]);
    }
}
