<?php

namespace App\Http\Controllers\Api\Post;

use App\Post;
use JWTAuthException;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class PostController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $posts = Post::all();
        
        return $this->showAll($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $user = JWTAuth::parseToken()->toUser();
            if(! $user){
                return response()->json(['error' => 'User not found'], 401);
            }
        } catch(\Tymon\JWTAutH\Exceptions\TokenInvalidException $ex){
            return response()->json(['error' => 'Token is invalid'], 401);
        } catch(\Tymon\JWTAutH\Exceptions\TokenExpiredException $ex){
            return response()->json(['error' => 'Token has expired'], 401);
        } catch(\Tymon\JWTAutH\Exceptions\TokenBlacklistedException $ex){
            return response()->json(['error' => 'Token is blacklisted'], 401);
        }

        // $rules = [
        //     'title' => 'required',
        //     'content' => 'required',
        // ];


        // $this->validate($request, $rules);

        if (!$request->has('title')) {
            return $this->errorResponse('The title field is required.', 422);
        }

        if (!$request->has('content')) {
            return $this->errorResponse('The content field is required.', 422);
        }


        $data = $request->all();

        $data['slug'] = str_slug($request->title);
        $data['user_id'] = $user->id;

        $post = Post::create($data);

        return $this->showOne($post);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try{
            $user = JWTAuth::parseToken()->toUser();
            if(! $user){
                return response()->json(['error' => 'User not found'], 401);
            }
        } catch(\Tymon\JWTAutH\Exceptions\TokenInvalidException $ex){
            return response()->json(['error' => 'Token is invalid'], 401);
        } catch(\Tymon\JWTAutH\Exceptions\TokenExpiredException $ex){
            return response()->json(['error' => 'Token has expired'], 401);
        } catch(\Tymon\JWTAutH\Exceptions\TokenBlacklistedException $ex){
            return response()->json(['error' => 'Token is blacklisted'], 401);
        }

        $rules = [
            'title' => 'required',
            'content' => 'required',
        ];

        $this->validate($request, $rules);

        $post = Post::findOrFail($id);

        if (!$request->has('title')) {
            return $this->errorResponse('The title field is required.', 422);
        }

        if (!$request->has('content')) {
            return $this->errorResponse('The content field is required.', 422);
        }
        
        if ($request->has('title')) {

            $post->title = $request->title;
            $post->slug  = str_slug($post->title);
        }

        if ($request->has('content')) {
            $post->content = $request->content;
        }

        if (!$post->isDirty()) {
            return $this->errorResponse('You need to specify a different value to update', 422);
        }

        $post->save();

        return $this->showOne($post);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);

        try{
            $user = JWTAuth::parseToken()->toUser();
            if(! $user){
                return response()->json(['error' => 'User not found'], 401);
            }
        } catch(\Tymon\JWTAutH\Exceptions\TokenInvalidException $ex){
            return response()->json(['error' => 'Token is invalid'], 401);
        } catch(\Tymon\JWTAutH\Exceptions\TokenExpiredException $ex){
            return response()->json(['error' => 'Token has expired'], 401);
        } catch(\Tymon\JWTAutH\Exceptions\TokenBlacklistedException $ex){
            return response()->json(['error' => 'Token is blacklisted'], 401);
        }

        if($post->user_id != $user->id)
        {
            return $this->errorResponse('Cannot Delete Post!', 422);
        }

        $post->delete();

        $message = 'record deleted successfully';

        return response()->json(['status' => $message]);
    }
}
