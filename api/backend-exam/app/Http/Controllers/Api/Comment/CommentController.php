<?php

namespace App\Http\Controllers\Api\Comment;

use App\Post;
use App\Comment;
use JWTAuthException;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;

class CommentController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        try{
            $user = JWTAuth::parseToken()->toUser();
            if(! $user){
                return response()->json(['error' => 'User not found'], 401);
            }
        } catch(\Tymon\JWTAutH\Exceptions\TokenInvalidException $ex){
            return response()->json(['error' => 'Token is invalid'], 401);
        } catch(\Tymon\JWTAutH\Exceptions\TokenExpiredException $ex){
            return response()->json(['error' => 'Token has expired'], 401);
        } catch(\Tymon\JWTAutH\Exceptions\TokenBlacklistedException $ex){
            return response()->json(['error' => 'Token is blacklisted'], 401);
        }

        // $rules = [
        //     'body' => 'required',
        // ];

        $post = Post::findOrFail($id);

        // $this->validate($request, $rules);

        if (!$request->has('body')) {
            return $this->errorResponse('The body field is required.', 422);
        }

        $data = $request->all();

        $data['creator_id'] = $user->id;
        $data['commentable_id'] = $post->id;

        $comment = Comment::create($data);

        return $this->showOne($comment);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $comment_id)
    {
         try{
            $user = JWTAuth::parseToken()->toUser();
            if(! $user){
                return response()->json(['error' => 'User not found'], 401);
            }
        } catch(\Tymon\JWTAutH\Exceptions\TokenInvalidException $ex){
            return response()->json(['error' => 'Token is invalid'], 401);
        } catch(\Tymon\JWTAutH\Exceptions\TokenExpiredException $ex){
            return response()->json(['error' => 'Token has expired'], 401);
        } catch(\Tymon\JWTAutH\Exceptions\TokenBlacklistedException $ex){
            return response()->json(['error' => 'Token is blacklisted'], 401);
        }

        // $rules = [
        //     'body' => 'required',
        // ];

        $post = Post::findOrFail($id);

        // $this->validate($request, $rules);

        if (!$request->has('body')) {
            return $this->errorResponse('The body field is required.', 422);
        }
        
        $comment = Comment::findOrFail($comment_id);

        if ($request->has('body')) {

            $comment->body = $request->body;
        }


        $comment->save();

        return $this->showOne($comment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $comment_id)
    {
        $post = Post::findOrFail($id);

        try{
            $user = JWTAuth::parseToken()->toUser();
            if(! $user){
                return response()->json(['error' => 'User not found'], 401);
            }
        } catch(\Tymon\JWTAutH\Exceptions\TokenInvalidException $ex){
            return response()->json(['error' => 'Token is invalid'], 401);
        } catch(\Tymon\JWTAutH\Exceptions\TokenExpiredException $ex){
            return response()->json(['error' => 'Token has expired'], 401);
        } catch(\Tymon\JWTAutH\Exceptions\TokenBlacklistedException $ex){
            return response()->json(['error' => 'Token is blacklisted'], 401);
        }

        $comment = Comment::findOrFail($comment_id);

        if($comment->creator_id != $user->id)
        {
            return $this->errorResponse('Cannot Delete Post!', 422);
        }

        $comment->delete();

        $message = 'record deleted successfully';

        return response()->json(['status' => $message]);
    }
}
