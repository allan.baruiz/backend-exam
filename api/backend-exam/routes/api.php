<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('auth/register', 'Api\Auth\UserController@register');
Route::post('auth/login', 'Api\Auth\UserController@login');
Route::group(['middleware' => 'jwt.auth'], function () {

	//User Details
    Route::get('user', 'Api\Auth\UserController@getAuthUser');

    //Posts Details
    Route::get('posts', 'Api\Post\PostController@index');
    Route::post('posts', 'Api\Post\PostController@store');
    Route::patch('posts/{id}', 'Api\Post\PostController@update');
    Route::delete('posts/{id}', 'Api\Post\PostController@destroy');

    //Comment
    Route::post('posts/{id}/comments', 'Api\Comment\CommentController@store');
    Route::patch('posts/{id}/comments/{comment_id}', 'Api\Comment\CommentController@update');
    Route::delete('posts/{id}/comments/{comment_id}', 'Api\Comment\CommentController@destroy');
    // Route::patch('posts/{id}', 'Api\Post\PostController@update');
    // Route::delete('posts/{id}', 'Api\Post\PostController@destroy');
});